# Online Journal## Maintainer

Group project at Experis Academy.
The group project main objective is to create a Singel-page-application (SPA) using nodejs and express.

## Installation

```bash
npm install 
```

## Development

```bash
npm run dev
```

Start both client and server, client runs on port 8080 and server 3001

```bash
npm run server
```

Start only server

```bash
npm run client
```

Start only client


## Production

```bash
npm start
```


## Maintainer

Team name: The Barrens

[@YuhXIV](https://github.com/YuhXIV) 
[@markusoxnevad1](https://gitlab.com/markusoxnevad1)
[mikkor14](https://gitlab.com/mikkor14)

