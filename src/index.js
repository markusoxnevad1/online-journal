const express = require("express");
const path = require("path");
const db = require("../models");
const bcrypt = require('bcrypt')
const passport = require('passport');
const LocalStrategy = require("passport-local").Strategy;
const session = require('express-session')
const cwd = process.cwd();
const app = express();

app.use(express.static(__dirname));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: 'keyboard cat',
    cookie: {
        httpOnly: true,
        maxAge: 3600000, // 1 hour
    }
}));

app.use(passport.initialize());
app.use(passport.session()); //this crash the program!?!?!??!?

// Instruct passport how to convert a user object into the corresponding primary key
passport.serializeUser((user, done) => done(null, user.id));

// Instruct passport how to fetch a user object from the database
passport.deserializeUser((id, done) => db.User.findByPk(id)
    .then((entry) => {
    done(null, entry);
}).catch(error => (done(error))));


passport.use(new LocalStrategy({
    // Field names to read off incoming requests
    usernameField: 'Username',
    passwordField: 'Password',

    // Remember authentication using sessions using previously configured express-session
    session: true,
    passReqToCallback: true,
}, async (req, Username, password, done) => {
    try {
        // Get stored bcrypt hash -- this will throw an error if not present.
        const stored = await db.User.findOne({ where: {Username: Username }});

        // Asynchronously compare the the hashed password
        const result = await new Promise((resolve, reject) => {
            bcrypt.compare(password, stored.Password, (err, success) => {
                if (err) {
                    return reject(err)
                }

                // Resolves true or false for comparison of the hash
                resolve(success);
            })
        });

        console.log(result);
        if (result) {
            return done(null, stored);
            //return done(null, stored)
        } else {
            return done(null, false, { message: 'Incorrect password.' })
        }

    } catch (err) {
        // User doesn't exist
        if (err.notFound) {
            return done(null, false, { message: 'User does not exist.' })
        }

        // Something _really_ bad happened... if you ever get here then you messed up your code.
        return done(err)
    }
}));

//START OF THE JOURNAL ENTRIES API

app.get("/api/getAllDamnEntries", (req, res) => {
     db.JournalEntry.findAll().then(entries => {
     res.status(200).json(entries.map(item => item.dataValues));
     });
});

//GET all entries in the DB
app.get("/api/entries", (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(401).json({ status: 'error' })
    }

    db.JournalEntry.findAll({where : {UserId:req.user.id}}).then(entries => {
        res.status(200).json(entries.map(item => item.dataValues));
    });

});

//GET entry by ID
app.get("/api/entries/:id", (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(401).json({ status: 'error' })
    }
    const { id } = req.params;
    let user = db.User.findByPk(req.User.email);

    db.JournalEntry.findOne({ where: { UserId: user.id, id: id } }).then(entry => {
        if (entry) {
            return res.status(200).json(entry.dataValues);
        }
        return res.status(404).end();
    }).catch(err => {
        console.error(err);
        res.status(400).end();
    });
});

//POST a new entry
app.post("/api/entries", (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(401).json({ status: 'error' })
    }

    const { body = {} } = req;
    const { Title, Subtitle, Text} = body;

    // Check if properties exist
    if (!Title) {
        return res.status(400).json({ error: "Title required" });
    }

    if (!Subtitle) {
        return res.status(400).json({ error: "Subtitle required" });
    }

    if (!Text) {
        return res.status(400).json({ error: "Text required" });
    }
    // Run database insert
    db.JournalEntry.create({ Title: Title, Subtitle: Subtitle, Text: Text, UserId: req.user.id })
        .then(result => {
            console.log("RESULT", result);
            res.status(201).json(result.dataValues);
        })
        .catch(err => console.error(err));
});

const upsert = (req, res) => {
    const { body = {} } = req;

    if (!body.id && req.params.id) {
        body.id = req.params.id;
    }

    const { id, Title, Subtitle, Text} = body;
    // Check if properties exist
    if (!id) {
        return res.status(400).json({ error: "id required" });
    }

    if (!Title) {
        return res.status(400).json({error: "Title required"});
    }

    if (!Subtitle) {
        return res.status(400).json({error: "Subtitle required"});
    }

    if (!Text) {
        return res.status(400).json({error: "Text required"});

    }

    db.JournalEntry.upsert(body, { returning: true }).then(entry =>
        res.status(200).json(entry.dataValues)
    );
};

//PUT request to update existing entry
app.put("/api/entries", upsert);
app.put("/api/entries/:id", upsert);

//DELETE request to remove an entry by ID
app.delete("/api/entries/:id", (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(401).json({ status: 'error' })
    }
    const { id } = req.params;

    db.JournalEntry.findOne({ where: { UserId: req.user.id, id: id } }).then(entry => {
        return entry.destroy();
    }).then(() => res.status(204).end());
});


//-----------------------------------------------------------------------------------------
//START OF USER API
//-----------------------------------------------------------------------------------------

app.get('/api/isLoggedInn', (req, res) => {
    if (!req.isAuthenticated()) {
        return res.status(404).json({ status: 'Not logged in' })
    } else {
        return res.status(200).json({ status: ' Is logged in' })
    }
})

app.post('/api/register', async (req, res, next) => {
    const { Username, Password } = req.body;
    const existing = await db.User.findOne({where: {Username: Username}});

    if (existing) {
        return res.status(403).json({ status: 'error', message: 'User already exists.' })
    }
    // User not found -- proceed with registration
    // Bcrypt config
    const saltRoundsString = process.env.SALT_ROUNDS;
    let saltRounds;
    if (saltRoundsString) {
        saltRounds = Number(saltRoundsString);
    } else {
        saltRounds = 10;
    }
    const hash = await bcrypt.hash(Password, saltRounds);
    // Store in db

    await db.User.create({
        Username: Username,
        Password: hash
    });
    return res.status(201).json({ status: 'ok', message: 'User registered. Please log in.' })
});

//POST
app.post('/api/login',
    passport.authenticate('local'),
    (req, res) => res.status(200).json({ status: 'ok', user: req.user }));


// Clear user session.
app.get('/api/logout', (req, res) => {
    // Shortcut to clear user session.
    req.logout();
    res.status(200).json({ status: 'ok' });
});

//GET all users
app.get("/api/users", (req, res) => {
    db.User.findAll().then(entries => {
        res.status(200).json(entries.map(item => item.dataValues));
    });
});

if (process.env.NODE_ENV === 'production') {
    // When in production mode: serve the built react app
    // When in development: the API is proxied from the create-react-app server.
    app.use(express.static(path.join(cwd, 'app', 'build')))
}


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on port ${port}.`));