import '../styles/index.scss';
import * as moment from 'moment';

let entries;

//Submits a new entry to the db
$('#submit-entry-button').click(function () {
    const entryTitle = $("#new-entry-title").val();
    const entrySubtitle = $("#new-entry-subtitle").val();
    const entryText = $("#new-entry-text").val();
    const entryDate = moment().format('LL');
    $.ajax({
        type: 'POST',
        url: `/api/entries`,
        data: {
            'Title': entryTitle,
            'Subtitle': entrySubtitle,
            'Text': entryText
        }
    }).done(() => rerenderData());
});

$( ".register-button" ).click(function () {
    const formData = getFormData($('#register-form'));
    $.ajax({
        type: 'POST',
        url: `/api/register`,
        data: {
            'Username': formData[0].value,
            'Password': formData[1].value,
        }
    }).done(() => rerenderData());
});

$( ".submit-button" ).click(function () {
    const formData = getFormData($('#login-form'));
    $.ajax({
        type: 'POST',
        url: `/api/login`,
        data: {
            'Username': formData[0].value,
            'Password': formData[1].value,
        }
    }).done(() => rerenderData());
});

$( ".logout-button" ).click(function () {
    $.ajax({
        type: 'GET',
        url: `/api/logout`,
    }).done(() => $("#entries").empty());

});

function getFormData(form) {
    return form.serializeArray();
}

//Creates an entry card
function createCard(id, title, subtitle, text, dateCreated, dateEdited) {
    return $(`        
        <div id="entry-${id}" class="col-sm-12 card-color mt-4">
            <div class="card card-content">
                <div class="card-body">
                    <h5 class="card-title" id="new-card-title">${title}</h5>
                    <h6 class="card-subtitle" id="new-card-subtitle">${subtitle}</h6>
                    <p class="card-text" id="new-card-text">${text}</p>
                    <p class="card-text" id="new-card-dateCreated">Created: ${moment(dateCreated)}</p>
                    <p class="card-text" id="new-card-dateCreated">Last edited: ${moment(dateEdited)}</p>
                    <button type="submit" class="btn btn-primary edit-card-button" data-toggle="modal" data-target="#exampleModal" value="${id}">Edit</button>
                </div>
            </div>
        </div>
    `);
}

function getFieldValues(id) {
    return entries.find(entry => entry.id == id);
}

function renderEntries(entries) {
    $("#entries").empty();
    document.getElementById("entries").innerHTML = "";
    entries.forEach(entry => {
        $("#entries").prepend(createCard(entry.id, entry.Title, entry.Subtitle, entry.Text, entry.createdAt, entry.updatedAt));

        //Updates the selected entry in the db
        $.each($('.save-changes-button'), (index, button) => {
            button.onclick = function (event) {
                $.ajax({
                    type: 'PUT',
                    url: `/api/entries/${document.getElementsByClassName("card-id")[0].innerHTML}`,
                    data: {
                        'Title': document.getElementById("modal-entry-title").value,
                        'Subtitle': document.getElementById("modal-entry-subtitle").value,
                        'Text': document.getElementById("modal-entry-text").value
                    }
                }).done(() => {
                    rerenderData();
                    $("#exampleModal").modal('hide');
                    window.location.reload();
                });
                window.setTimeout(function() {window.location.reload();}, 1000);
            };
        });

        //Deletes the select entry from the db
        $.each($('.delete-card-button'), (index, button) => {
            button.onclick = function (event) {
                $.ajax({
                    type: 'DELETE',
                    url: `/api/entries/${document.getElementsByClassName("card-id")[0].innerHTML}`,
                }).done(() => rerenderData());
                $("#exampleModal").modal('hide');
            };
        });

        //Open a modal for updating/deleting the selected entry
        $.each($('.edit-card-button'), (index, button) => {
            button.onclick = function (event) {
                const cardId = event.target.parentNode.parentNode.parentNode.id;
                const id = cardId.replace('entry-', '');
                console.log(id);
                const entry = getFieldValues(id);
                $('#modal-form').remove();
                $('#edit-body').append($(`
	<form id="modal-form">
	    <div class="card-id" style="visibility: hidden">${id}</div>
        <div class="form-group wrapper">
            <input id="modal-entry-title" value="${entry.Title}" class="input col-sm-12" required placeholder="Title" type="text" >
            <input id="modal-entry-subtitle" value="${entry.Subtitle}" class="input col-sm-12" placeholder="Subtitle" type="text" >
        </div>
        <div class="form-group">
            <label for="modal-entry-text" id="modal-description">Description</label>
			<textarea class="form-control card-content underline" required id="modal-entry-text" rows="3">${entry.Text}</textarea>
		</div> 
	</form>
	`));
            };
        });
    });
}

$(document).ready(() => {
    rerenderData();
});

function rerenderData() {
    $.ajax({
        type: 'GET',
        url: '/api/entries'
    }).done(data => {
        entries = data;
        renderEntries(data);
    });
    console.log("Data rerendered");
};


/**
 * For more information on using Moment head to https://momentjs.com/
 */