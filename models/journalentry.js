'use strict';
module.exports = (sequelize, DataTypes) => {
  const JournalEntry = sequelize.define('JournalEntry', {
    Title: DataTypes.STRING,
    Subtitle: DataTypes.STRING,
    Text: DataTypes.TEXT,
    UserId: DataTypes.INTEGER
  }, {});
  JournalEntry.associate = function(models) {
    // associations can be defined here
  };
  return JournalEntry;
};